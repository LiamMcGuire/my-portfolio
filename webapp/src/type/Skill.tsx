import {jsonToSkillType, SkillType} from "./SkillType";

export class Skill {

    readonly id: number;
    readonly skillType: SkillType;
    readonly name: string;
    readonly level: number;

    constructor(id: number, skillType: SkillType, name: string, level: number) {
        this.id = id;
        this.skillType = skillType;
        this.name = name;
        this.level = level;
    }
}

export const jsonToSkill = (json: any) =>
    new Skill(json["_id"], jsonToSkillType(json["skillType"]), json["name"], json["level"]);