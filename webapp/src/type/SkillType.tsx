export class SkillType {

    readonly description: string;

    constructor(description: string) {
        this.description = description;
    }
}

export const jsonToSkillType = (json: any) => new SkillType(json["description"]);