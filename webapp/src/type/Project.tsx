export class Project {

    readonly id: number;
    readonly title: string;
    readonly description: string;
    readonly shortDescription: string;
    readonly repositoryLink: string;
    readonly dateCompleted: number;
    readonly latestProject: boolean;

    constructor(
        id: number,
        title: string,
        description: string,
        shortDescription: string,
        repositoryLink: string,
        dateCompleted: number,
        latestProject: boolean
    ) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.shortDescription = shortDescription;
        this.repositoryLink = repositoryLink;
        this.dateCompleted = dateCompleted;
        this.latestProject = latestProject;
    }
}

export const jsonToProject = (json: any) => new Project(
    json["_id"],
    json["title"],
    json["description"],
    json["shortDescription"],
    json["repositoryLink"],
    json["dateCompleted"],
    json["latestProject"]
);