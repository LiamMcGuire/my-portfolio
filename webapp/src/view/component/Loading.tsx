import React from "react";
import {Card, Image} from "react-bootstrap";
import spinner from "../images/spinner.svg";

const Loading = () => <div className="width-handler-mini buffer-top">
    <Card className="text-center">
        <Card.Body>
            <Image src={spinner}/>
        </Card.Body>
    </Card>
</div>;

export default Loading;