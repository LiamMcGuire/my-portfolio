import React from "react";
import {Alert} from "react-bootstrap"

interface DismissibleAlertProps {
    dismissAlert: () => void
    variant: "danger" | "success"
    message: string | Array<String>
}

const SuccessHeader = () => <Alert.Heading>Success!</Alert.Heading>;
const FailureHeader = () => <Alert.Heading>Oh snap! You got an error!</Alert.Heading>;

const DismissibleAlert = (props: DismissibleAlertProps) =>
    <Alert variant={props.variant} onClose={() => props.dismissAlert()} dismissible>
        {props.variant === "danger" ? FailureHeader() : SuccessHeader()}
        {typeof props.message === "string" ? <p>{props.message}</p> : props.message.map((message => <p>{message}</p>))}
    </Alert>;

export default DismissibleAlert;