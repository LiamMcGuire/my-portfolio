import React from "react";

import {Card} from "react-bootstrap";

const PageNotFound = () => <div className="width-handler-medium buffer-top">
    <Card className="text-center">
        <Card.Body>
            <h1 style={{fontSize: 180}}>404</h1>
            <h4>Page Not Found</h4>
        </Card.Body>
    </Card>
</div>;

export default PageNotFound;