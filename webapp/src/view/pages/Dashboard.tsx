import React from "react";
import {Col, Container, Image, Row} from "react-bootstrap";
import profile from "../images/profile.png"

const Dashboard = () => <div className="width-handler-medium buffer-top buffer-bottom">
    <Container className="text-center">
        <Row style={{display: "inline-block"}}>
            <Col xs={6} className="text-center">
                <Image src={profile} height="350" width="350" roundedCircle/>
                <h1 style={{width: 350}}>I'm Liam McGuire</h1>
            </Col>
        </Row>
        <br/><br/><br/>
        <Row>
            <Col>
                <p className="text-center">
                    I am a highly motivated software developer with just over two years of industry experience and a
                    demonstrated history of producing high quality, maintainable software. During my time in industry, I
                    have developed a keen interest in both functional and object orientated programming. This has
                    allowed me to work on a range of applications and learn interesting technologies and programming
                    languages such as Scala and Spring WebFlux.
                </p>
                <br/>
                <p className="text-center">
                    In my current role, I am the SME for two applications but also provide support to an array of other
                    applications across multiple technology domains. This includes developing new features, finding and
                    fixing bugs, developing automated testing suites and integrating applications with third-party
                    services. Whilst working on these applications, I have been exposed to multiple architectures,
                    programming languages and frameworks. Such exposure has allowed me to master several technologies of
                    which I have been able to apply to new applications and personal projects.
                </p>
                <br/>
                <p>
                    In my spare time, I enjoy enhancing my skills by researching and playing around with previously
                    unknown and emerging technologies such as Spring Data REST and the R2DBC driver. Additionally I
                    enjoy developing small software projects with technologies I can later present at work which may or
                    may not be adopted. In the future, I intend to branch from this and start getting involved in open
                    source software.
                </p>
            </Col>
        </Row>
    </Container>
</div>;

export default Dashboard;