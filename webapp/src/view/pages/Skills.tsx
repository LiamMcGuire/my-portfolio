import React from "react";
import axios from "axios";
import {jsonToSkill, Skill} from "../../type/Skill";
import Loading from "../component/Loading";
import {Col, Image, Row} from "react-bootstrap";
import {ThumbnailMapping} from "../../utility/image-mapping";
import {chunk} from "../../utility/array-utilities";

const ENDPOINT_LIST_SKILLS: string = "http://localhost:8080/api/v1/skill/list";

interface Props {
}

interface State {
    skills: Array<Skill>
}

export default class Skills extends React.Component<Props, State> {

    state: State = {
        skills: []
    };

    componentDidMount() {
        axios.get(ENDPOINT_LIST_SKILLS)
            .then((response) => this.setState({
                skills: response.data.map((skillJson: any) => jsonToSkill(skillJson))
            }))
            .catch((failure) => console.log(failure))
    }

    render() {
        const displaySkills = (skills: Skill[][]) => skills.map((skillChunks) =>
            <Row className="buffer-top">
                {skillChunks.map((skill) =>
                    <Col xs={6}>
                        <Row>
                            <Col xs={5}>
                                <Image src={
                                    // @ts-ignore
                                    ThumbnailMapping[skill.name.replace(/[\n\r\s\t]+/g, '').toLowerCase()]
                                } height="200" width="200"/>
                            </Col>
                            <Col xs={7}>
                                <h3>{skill.name}</h3>
                                <div>
                                    {Array.from({length: skill.level}, (v, k) => k + 1).map(() =>
                                        <i className="fas fa-star"/>
                                    )}
                                    {Array.from({length: 10 - skill.level}, (v, k) => k + 1).map(() =>
                                        <i className="far fa-star"/>
                                    )}
                                </div>
                            </Col>
                        </Row>
                    </Col>
                )}
            </Row>
        );

        if (this.state.skills.length === 0) {
            return (<Loading/>)
        } else {
            return (
                <div className="width-handler-medium buffer-top buffer-bottom">
                    {["Programming Languages", "Framework", "Database Technologies", "DevOps", "Operating Systems"]
                        .map((category) =>
                            <div>
                                <Row>
                                    <Col>
                                        <h1>{category}</h1>
                                        <hr/>
                                        {displaySkills(chunk(this.state.skills.filter((skill) => skill.skillType.description === category), 2))}
                                    </Col>
                                </Row>
                                <br/><br/><br/>
                            </div>
                        )
                    }
                </div>
            )
        }
    }
}