import React from "react";
import axios from "axios";
import {jsonToProject, Project} from "../../type/Project";
import {Card} from "react-bootstrap";
import Loading from "../component/Loading";

const ENDPOINT_VIEW_PROJECT = (projectId: string) => "http://localhost:8080/api/v1/project/" + projectId + "/view";

interface Props {
    match: any
}

interface State {
    project: Project | undefined
}

export default class ProjectDetail extends React.Component<Props, State> {

    state: State = {
        project: undefined
    };

    componentDidMount() {
        axios.get(ENDPOINT_VIEW_PROJECT(this.props.match.params.id))
            .then((response) => this.setState({
                project: jsonToProject(response.data)
            }))
            .catch((error) => console.log(error))
    }

    render() {
        if (this.state.project === undefined) {
            return (<Loading/>)
        } else {
            return (
                <div className="width-handler-large buffer-top">
                    <Card>
                        <Card.Body>
                            <h1 className="text-center">{this.state.project?.title}</h1>
                            <br/>
                            <div className="text-center">
                                <p style={{wordWrap: "break-word", width: 900, display: "inline-block"}}>
                                    {this.state.project?.description}
                                </p>
                            </div>
                            <br/>
                            <hr/>
                            <p>Completion Date: {this.state.project?.dateCompleted}</p>
                            <p><a href={this.state.project?.repositoryLink}>GitLab Repository</a></p>
                        </Card.Body>
                    </Card>
                </div>
            )
        }
    }
}