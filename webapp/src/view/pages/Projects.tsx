import React from "react";
import axios from "axios";
import {Col, Jumbotron, Row} from "react-bootstrap";
import {jsonToProject, Project} from "../../type/Project";
import Loading from "../component/Loading";
import {chunk} from "../../utility/array-utilities";

const ENDPOINT_LIST_PROJECTS: string = "http://localhost:8080/api/v1/project/list";

interface Props {
}

interface State {
    projects: Array<Project>
}

export default class Projects extends React.Component<Props, State> {

    state: State = {
        projects: []
    };

    componentDidMount() {
        axios.get(ENDPOINT_LIST_PROJECTS)
            .then((response) => this.setState({
                projects: response.data.map((projectJson: any) => jsonToProject(projectJson))
            }))
            .catch((error) => console.log(error))
    }

    render() {
        const latest: Project = this.state.projects.filter((project) => project.latestProject)[0];
        const projectsMinusLatest: Project[][] =
            chunk(this.state.projects.filter((project) => project !== latest), 2);

        if (this.state.projects.length === 0) {
            return (<Loading/>)
        } else {
            return (
                <div className="width-handler-large buffer-top">
                    <Row>
                        <Col>
                            <Jumbotron className="rounded bg-dark p-4 text-white bg-dark">
                                <Col xs={6}>
                                    <h1 className="display-4 font-italic">{latest.title}</h1>
                                    <p><a className="lead my-3" href={latest.repositoryLink}>GitLab Repository</a></p>
                                    <p className="lead my-3">{latest.shortDescription}</p>
                                    <p className="lead mb-0">
                                        <a href={"project/" + latest.id} className="text-white font-weight-bold">
                                            Continue reading...
                                        </a>
                                    </p>
                                </Col>
                            </Jumbotron>
                        </Col>
                    </Row>
                    {projectsMinusLatest.map((projectChunks) =>
                        <Row>
                            {projectChunks.map((project) =>
                                <Col xs={6}>
                                    <Row
                                        className="no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                                        <Col xs={9} className="p-4 d-flex flex-column position-static">
                                            <h3 className="mb-0">{project.title}</h3>
                                            <div className="mb-1 text-muted">{project.dateCompleted}</div>
                                            <p>
                                                <a className="lead my-3" href={latest.repositoryLink}>
                                                    GitLab Repository
                                                </a>
                                            </p>
                                            <p className="card-text mb-auto">{project.shortDescription}</p>
                                            <a href={"project/" + project.id} className="stretched-link">
                                                Continue reading
                                            </a>
                                        </Col>
                                        <Col xs={3} className="d-none d-lg-block">
                                            <svg className="bd-placeholder-img" width="200" height="250"
                                                 xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice"
                                                 focusable="false" role="img">
                                                <rect width="100%" height="100%" fill="#55595c"/>
                                            </svg>
                                        </Col>
                                    </Row>
                                </Col>
                            )}
                        </Row>
                    )}
                </div>
            )
        }
    }
}