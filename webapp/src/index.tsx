import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {Nav, Navbar} from "react-bootstrap";
import PageNotFound from "./view/pages/error/PageNotFound";
import Dashboard from "./view/pages/Dashboard";
import Projects from "./view/pages/Projects";
import Skills from "./view/pages/Skills";
import "./view/styles/index.css";
import ProjectDetail from "./view/pages/ProjectDetail";

const App = () => <BrowserRouter>
    <div>
        <div className="navbar-wrapper sticky-top">
            <Navbar className="width-handler-large" bg="dark" variant="dark" expand="lg">
                <Navbar.Brand>Liam McGuire</Navbar.Brand>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link href="/dashboard">Home</Nav.Link>
                        <Nav.Link href="/projects">Projects</Nav.Link>
                        <Nav.Link href="/skills">Skills</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse className="justify-content-end">
                    <Nav className="mt-auto">
                        <Nav.Link href={window.location.origin + "/resume.pdf"}>
                            CV
                        </Nav.Link>
                        <Nav.Link href="https://www.linkedin.com/in/liam-mcguire-6a6aa319a/">
                            LinkedIn
                        </Nav.Link>
                        <Nav.Link href="https://gitlab.com/LiamMcGuire">GitLab</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </div>
        <Switch>
            <Route exact path="/" component={Dashboard}/>
            <Route exact path="/dashboard" component={Dashboard}/>
            <Route exact path="/projects" component={Projects}/>
            <Route exact path="/project/:id" component={ProjectDetail}/>
            <Route exact path="/skills" component={Skills}/>
            <Route exact path="*" component={PageNotFound}/>
        </Switch>
    </div>
</BrowserRouter>;

ReactDOM.render(<App/>, document.getElementById("root"));