export const chunk = (array: Array<any>, length: number) => {
    let chunks = [], iteration = 0;
    while (iteration < array.length) {
        chunks.push(array.slice(iteration, iteration += length))
    }
    return chunks;
};