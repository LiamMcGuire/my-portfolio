import kotlin from "../view/images/logo.kotlin.png"
import java from "../view/images/logo.java.png"
import javascript from "../view/images/logo.javascript.jpeg"
import python from "../view/images/logo.python.png"
import scala from "../view/images/logo.scala.png"
import typescript from "../view/images/logo.typescript.png"
import spring from "../view/images/logo.spring.png"
import react from "../view/images/logo.react.png"
import django from "../view/images/logo.django.png"
import hibernate from "../view/images/logo.hibernate.png"
import postgresql from "../view/images/logo.postgres.png"
import mongodb from "../view/images/logo.mongo.png"
import maven from "../view/images/logo.maven.png"
import linux from "../view/images/logo.linux.png"
import git from "../view/images/logo.git.png"
import sbt from "../view/images/logo.sbt.png"
import docker from "../view/images/logo.docker.png"
import dockerCompose from "../view/images/logo.docker-compose.png"
import svn from "../view/images/logo.svn.png"
import windows from "../view/images/logo.windows.png"
import gradle from "../view/images/logo.gradle.png"
import keycloak from "../view/images/logo.keycloak.png"
import prometheus from "../view/images/logo.prometheus.png"
import grafana from "../view/images/logo.grafana.png"
import atlassian from "../view/images/logo.atlassian.png"
import googleappengine from "../view/images/logo.app-engine.png"
import googlecomputeengine from "../view/images/logo.compute-engine.png"
import android from "../view/images/logo.android.png"

export const ThumbnailMapping = {
    "kotlin": kotlin,
    "java": java,
    "javascript": javascript,
    "python": python,
    "scala": scala,
    "typescript": typescript,
    "springmvc": spring,
    "springcloud": spring,
    "springwebflux": spring,
    "springdatarest": spring,
    "reactjs": react,
    "django": django,
    "hibernate": hibernate,
    "hql": hibernate,
    "postgresql": postgresql,
    "mongodb": mongodb,
    "maven": maven,
    "linux": linux,
    "git": git,
    "sbt": sbt,
    "docker": docker,
    "dockercompose": dockerCompose,
    "svn": svn,
    "windows": windows,
    "gradle": gradle,
    "atlassiansuite(jira,bamboo,stash)": atlassian,
    "keycloak": keycloak,
    "prometheus": prometheus,
    "grafana": grafana,
    "googleappengine": googleappengine,
    "googlecomputeengine": googlecomputeengine,
    "android": android
};