package me.liammcguire.myportfolio.handler

import com.fasterxml.jackson.databind.JsonNode
import com.mongodb.client.result.DeleteResult
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import me.liammcguire.myportfolio.model.Project
import me.liammcguire.myportfolio.repository.ProjectRepository
import me.liammcguire.myportfolio.validation.ProjectValidator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@ExtendWith(SpringExtension::class)
@WebFluxTest(ProjectHandler::class)
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
internal class ProjectHandlerTest {

    private val mockProjectRepository: ProjectRepository = mockk()
    private val projectValidator = ProjectValidator()
    private val handler = ProjectHandler(mockProjectRepository, projectValidator)

    private val webTestClient = WebTestClient.bindToRouterFunction(projectRoutes(handler)).build()

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Creating a project should return a 202 response`() {
        val project = createProject()
        every { mockProjectRepository.store(any()) } returns Mono.just(project.copy(
            _id = "5e2234051dfd0b58632c48ed"
        ))

        val responseBody = webTestClient.post()
            .uri("/api/v1/project")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(project)
            .exchange()
            .expectStatus().isCreated
            .expectBody(Project::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        assertEquals(project.copy(_id = "5e2234051dfd0b58632c48ed").hashCode(), responseBody!!.hashCode())

        verify(exactly = 1) { mockProjectRepository.store(any()) }
    }

    @Test
    internal fun `Creating a project should return a 400 response when no JSON body is provided`() {
        val responseBody = webTestClient.post()
            .uri("/api/v1/project")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isBadRequest
            .expectBody(JsonNode::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val error = responseBody!!.get("errors").map { it.get("cause").textValue() }.firstOrNull()
        assertNotNull(error)
        assertEquals("The request did not have a JSON body.", error)

        verify(exactly = 0) { mockProjectRepository.store(any()) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Creating a project should return a 400 response when the JSON body is invalid`() {
        val project = Project(description = "someDescription", repositoryLink = "10jdasomd 01-")

        val responseBody = webTestClient.post()
            .uri("/api/v1/project")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(project)
            .exchange()
            .expectStatus().isBadRequest
            .expectBody(JsonNode::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val expectedErrors = listOf(
            "The provided repository link was not valid.",
            "A title was expected but was not provided.",
            "A short description was expected but was not provided."
        )
        responseBody!!.get("errors").forEach { node -> assertTrue(expectedErrors.contains(node.get("cause").textValue())) }

        verify(exactly = 0) { mockProjectRepository.store(any()) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Updating a project should return a 200 response`() {
        val project = createProject().copy(_id = "5e2234051dfd0b58632c48ed")
        val updateProject = Project(title = "updatedTitle", description = "updateDescription")
        val combinedProjects = project.copy(title = updateProject.title, description = updateProject.description)
        every { mockProjectRepository.fetchById("5e2234051dfd0b58632c48ed") } returns Mono.just(project)
        every { mockProjectRepository.store(any()) } returns Mono.just(combinedProjects)

        val responseBody = webTestClient.patch()
            .uri("/api/v1/project/5e2234051dfd0b58632c48ed/update")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(updateProject)
            .exchange()
            .expectStatus().isOk
            .expectBody(Project::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        assertEquals(combinedProjects.hashCode(), responseBody!!.hashCode())

        verify(exactly = 1) {
            mockProjectRepository.fetchById("5e2234051dfd0b58632c48ed")
            mockProjectRepository.store(any())
        }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Updating a project should return a 404 response when a project cannot be found`() {
        val updateProject = Project(title = "updatedTitle", description = "updateDescription")
        every { mockProjectRepository.fetchById("5e2234051dfd0b58632c48ed") } returns Mono.empty()

        webTestClient.patch()
            .uri("/api/v1/project/5e2234051dfd0b58632c48ed/update")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(updateProject)
            .exchange()
            .expectStatus().isNotFound
            .expectBody().isEmpty

        verify(exactly = 1) { mockProjectRepository.fetchById("5e2234051dfd0b58632c48ed") }
        verify(exactly = 0) { mockProjectRepository.store(any()) }
    }

    @Test
    internal fun `Updating a project should return a 400 response when no JSON body is provided`() {
        val responseBody = webTestClient.patch()
            .uri("/api/v1/project/5e2234051dfd0b58632c48ed/update")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isBadRequest
            .expectBody(JsonNode::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val error = responseBody!!.get("errors").map { it.get("cause").textValue() }.firstOrNull()
        assertNotNull(error)
        assertEquals("The request did not have a JSON body.", error)

        verify(exactly = 0) {
            mockProjectRepository.store(any())
            mockProjectRepository.fetchById("5e2234051dfd0b58632c48ed")
        }
    }

    @Test
    internal fun `Updating a project should return a 400 response when the JSON body is invalid`() {
        val updateProject = Project(description = "someDescription", repositoryLink = "10jdasomd 01-")

        val responseBody = webTestClient.patch()
            .uri("/api/v1/project/5e2234051dfd0b58632c48ed/update")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(updateProject)
            .exchange()
            .expectStatus().isBadRequest
            .expectBody(JsonNode::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val expectedErrors = listOf("The provided repository link was not valid.")
        responseBody!!.get("errors").forEach { node -> assertTrue(expectedErrors.contains(node.get("cause").textValue())) }

        verify(exactly = 0) {
            mockProjectRepository.fetchById("5e2234051dfd0b58632c48ed")
            mockProjectRepository.store(any())
        }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Listing all projects should return a 200 response`() {
        val projects = arrayOf(
            createProject().copy(_id = "5e2234051dfd0b58632c48ed"),
            createProject().copy(_id = "5e2234051dfd0b58632c48ee"),
            createProject().copy(_id = "5e2234051dfd0b58632c48ef")
        )
        val hashCodes = projects.map { it.hashCode() }
        every { mockProjectRepository.list() } returns Flux.fromArray(projects)

        val responseBody = webTestClient.get()
            .uri("/api/v1/project/list")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBodyList(Project::class.java)
            .hasSize(3)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        responseBody!!.forEach { project -> assertTrue(hashCodes.contains(project.hashCode())) }

        verify(exactly = 1) { mockProjectRepository.list() }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Listing all projects should return a 404 response when no projects can be found`() {
        every { mockProjectRepository.list() } returns Flux.empty()

        webTestClient.get()
            .uri("/api/v1/project/list")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
            .expectBody().isEmpty

        verify(exactly = 1) { mockProjectRepository.list() }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Viewing a project should return a 200 response`() {
        val project = createProject().copy(_id = "5e2234051dfd0b58632c48ed")
        every { mockProjectRepository.fetchById("5e2234051dfd0b58632c48ed") } returns Mono.just(project)

        val responseBody = webTestClient.get()
            .uri("/api/v1/project/5e2234051dfd0b58632c48ed/view")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody(Project::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        assertEquals(project.hashCode(), responseBody!!.hashCode())

        verify(exactly = 1) { mockProjectRepository.fetchById("5e2234051dfd0b58632c48ed") }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Viewing a project should return a 404 response when a project cannot be found`() {
        every { mockProjectRepository.fetchById("5e2234051dfd0b58632c48ed") } returns Mono.empty()

        webTestClient.get()
            .uri("/api/v1/project/5e2234051dfd0b58632c48ed/view")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
            .expectBody().isEmpty

        verify(exactly = 1) { mockProjectRepository.fetchById("5e2234051dfd0b58632c48ed") }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Deleting a project should return a 200 response`() {
        val project = createProject().copy(_id = "5e2234051dfd0b58632c48ed")
        every { mockProjectRepository.fetchById("5e2234051dfd0b58632c48ed") } returns Mono.just(project)
        every { mockProjectRepository.remove(project) } returns Mono.just(DeleteResult.acknowledged(1))

        webTestClient.delete()
            .uri("/api/v1/project/5e2234051dfd0b58632c48ed/delete")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectBody().isEmpty

        verify(exactly = 1) {
            mockProjectRepository.fetchById("5e2234051dfd0b58632c48ed")
            mockProjectRepository.remove(project)
        }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Deleting a project should return a 404 response when the project cannot be found`() {
        every { mockProjectRepository.fetchById("5e2234051dfd0b58632c48ed") } returns Mono.empty()

        webTestClient.delete()
            .uri("/api/v1/project/5e2234051dfd0b58632c48ed/delete")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
            .expectBody().isEmpty

        verify(exactly = 1) { mockProjectRepository.fetchById("5e2234051dfd0b58632c48ed") }
        verify(exactly = 0) { mockProjectRepository.remove(any()) }
    }

    companion object {
        fun createProject(): Project = Project(
            title = "someTitle",
            description = "someDescription",
            shortDescription = "someShortDescription",
            repositoryLink = "http://localhost.com",
            dateCompleted = null
        )
    }
}
