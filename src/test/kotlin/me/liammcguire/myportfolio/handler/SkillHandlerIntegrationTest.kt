package me.liammcguire.myportfolio.handler

import com.fasterxml.jackson.databind.JsonNode
import com.mongodb.client.result.DeleteResult
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import me.liammcguire.myportfolio.model.Skill
import me.liammcguire.myportfolio.model.SkillType
import me.liammcguire.myportfolio.repository.SkillRepository
import me.liammcguire.myportfolio.validation.SkillValidator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@ExtendWith(SpringExtension::class)
@WebFluxTest(SkillHandler::class)
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
internal class SkillHandlerIntegrationTest {

    private val mockSkillRepository: SkillRepository = mockk()
    private val skillValidator = SkillValidator()
    private val handler = SkillHandler(mockSkillRepository, skillValidator)

    private val webTestClient = WebTestClient.bindToRouterFunction(skillRoutes(handler)).build()

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Creating a skill should return a 202 response`() {
        val skill = createSkill()
        every { mockSkillRepository.store(any()) } returns Mono.just(skill.copy(
            _id = "5e2234051dfd0b58632c48ed"
        ))

        val responseBody = webTestClient.post()
            .uri("/api/v1/skill")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(skill)
            .exchange()
            .expectStatus().isCreated
            .expectBody(Skill::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        assertEquals(skill.copy(_id = "5e2234051dfd0b58632c48ed").hashCode(), responseBody!!.hashCode())

        verify(exactly = 1) { mockSkillRepository.store(any()) }
    }

    @Test
    internal fun `Creating a skill should return a 400 response when no JSON body is provided`() {
        val responseBody = webTestClient.post()
            .uri("/api/v1/skill")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isBadRequest
            .expectBody(JsonNode::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val error = responseBody!!.get("errors").map { it.get("cause").textValue() }.firstOrNull()
        assertNotNull(error)
        assertEquals("The request did not have a JSON body.", error)

        verify(exactly = 0) { mockSkillRepository.store(any()) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Creating a skill should return a 400 response when the JSON body is invalid`() {
        val skill = Skill(level = 20, name = "")

        val responseBody = webTestClient.post()
            .uri("/api/v1/skill")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(skill)
            .exchange()
            .expectStatus().isBadRequest
            .expectBody(JsonNode::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val expectedErrors = listOf(
            "A name was expected but was not provided.",
            "A skill type was expected but was not provided.",
            "A description was expected but was not provided.",
            "The provided level was not between 0 and 10."
        )
        responseBody!!.get("errors").forEach { node -> assertTrue(expectedErrors.contains(node.get("cause").textValue())) }

        verify(exactly = 0) { mockSkillRepository.store(any()) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Updating a skill should return a 200 response`() {
        val skill = createSkill().copy(_id = "5e2234051dfd0b58632c48ed")
        val updateSkill = Skill(name = "updateName")
        val combinedSkills = skill.copy(name = updateSkill.name)
        every { mockSkillRepository.fetchById("5e2234051dfd0b58632c48ed") } returns Mono.just(skill)
        every { mockSkillRepository.store(any()) } returns Mono.just(combinedSkills)

        val responseBody = webTestClient.patch()
            .uri("/api/v1/skill/5e2234051dfd0b58632c48ed/update")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(updateSkill)
            .exchange()
            .expectStatus().isOk
            .expectBody(Skill::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        assertEquals(combinedSkills.hashCode(), responseBody!!.hashCode())

        verify(exactly = 1) {
            mockSkillRepository.fetchById("5e2234051dfd0b58632c48ed")
            mockSkillRepository.store(any())
        }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Updating a skill should return a 404 response when a skill cannot be found`() {
        val updateSkill = Skill(name = "updateName")
        every { mockSkillRepository.fetchById("5e2234051dfd0b58632c48ed") } returns Mono.empty()

        webTestClient.patch()
            .uri("/api/v1/skill/5e2234051dfd0b58632c48ed/update")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(updateSkill)
            .exchange()
            .expectStatus().isNotFound
            .expectBody().isEmpty

        verify(exactly = 1) { mockSkillRepository.fetchById("5e2234051dfd0b58632c48ed") }
        verify(exactly = 0) { mockSkillRepository.store(any()) }
    }

    @Test
    internal fun `Updating a skill should return a 400 response when no JSON body is provided`() {
        val responseBody = webTestClient.patch()
            .uri("/api/v1/skill/5e2234051dfd0b58632c48ed/update")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isBadRequest
            .expectBody(JsonNode::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val error = responseBody!!.get("errors").map { it.get("cause").textValue() }.firstOrNull()
        assertNotNull(error)
        assertEquals("The request did not have a JSON body.", error)

        verify(exactly = 0) {
            mockSkillRepository.store(any())
            mockSkillRepository.fetchById("5e2234051dfd0b58632c48ed")
        }
    }

    @Test
    internal fun `Updating a skill should return a 400 response when the JSON body is invalid`() {
        val updateSkill = Skill(level = 20)

        val responseBody = webTestClient.patch()
            .uri("/api/v1/skill/5e2234051dfd0b58632c48ed/update")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(updateSkill)
            .exchange()
            .expectStatus().isBadRequest
            .expectBody(JsonNode::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val expectedErrors = listOf("The provided level was not between 0 and 10.")
        responseBody!!.get("errors").forEach { node -> assertTrue(expectedErrors.contains(node.get("cause").textValue())) }

        verify(exactly = 0) {
            mockSkillRepository.fetchById("5e2234051dfd0b58632c48ed")
            mockSkillRepository.store(any())
        }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Listing all skills should return a 200 response`() {
        val skills = arrayOf(
            createSkill().copy(_id = "5e2234051dfd0b58632c48ed"),
            createSkill().copy(_id = "5e2234051dfd0b58632c48ee"),
            createSkill().copy(_id = "5e2234051dfd0b58632c48ef")
        )
        val hashCodes = skills.map { it.hashCode() }
        every { mockSkillRepository.list() } returns Flux.fromArray(skills)

        val responseBody = webTestClient.get()
            .uri("/api/v1/skill/list")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBodyList(Skill::class.java)
            .hasSize(3)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        responseBody!!.forEach { skill -> assertTrue(hashCodes.contains(skill.hashCode())) }

        verify(exactly = 1) { mockSkillRepository.list() }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Listing all skills should return a 404 response when no skills can be found`() {
        every { mockSkillRepository.list() } returns Flux.empty()

        webTestClient.get()
            .uri("/api/v1/skill/list")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
            .expectBody().isEmpty

        verify(exactly = 1) { mockSkillRepository.list() }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Viewing a skill should return a 200 response`() {
        val skill = createSkill().copy(_id = "5e2234051dfd0b58632c48ed")
        every { mockSkillRepository.fetchById("5e2234051dfd0b58632c48ed") } returns Mono.just(skill)

        val responseBody = webTestClient.get()
            .uri("/api/v1/skill/5e2234051dfd0b58632c48ed/view")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody(Skill::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        assertEquals(skill.hashCode(), responseBody!!.hashCode())

        verify(exactly = 1) { mockSkillRepository.fetchById("5e2234051dfd0b58632c48ed") }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Viewing a skill should return a 404 response when a skill cannot be found`() {
        every { mockSkillRepository.fetchById("5e2234051dfd0b58632c48ed") } returns Mono.empty()

        webTestClient.get()
            .uri("/api/v1/skill/5e2234051dfd0b58632c48ed/view")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
            .expectBody().isEmpty

        verify(exactly = 1) { mockSkillRepository.fetchById("5e2234051dfd0b58632c48ed") }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Deleting a skill should return a 200 response`() {
        val skill = createSkill().copy(_id = "5e2234051dfd0b58632c48ed")
        every { mockSkillRepository.fetchById("5e2234051dfd0b58632c48ed") } returns Mono.just(skill)
        every { mockSkillRepository.remove(skill) } returns Mono.just(DeleteResult.acknowledged(1))

        webTestClient.delete()
            .uri("/api/v1/skill/5e2234051dfd0b58632c48ed/delete")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectBody().isEmpty

        verify(exactly = 1) {
            mockSkillRepository.fetchById("5e2234051dfd0b58632c48ed")
            mockSkillRepository.remove(skill)
        }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Deleting a skill should return a 404 response when the skill cannot be found`() {
        every { mockSkillRepository.fetchById("5e2234051dfd0b58632c48ed") } returns Mono.empty()

        webTestClient.delete()
            .uri("/api/v1/skill/5e2234051dfd0b58632c48ed/delete")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
            .expectBody().isEmpty

        verify(exactly = 1) { mockSkillRepository.fetchById("5e2234051dfd0b58632c48ed") }
        verify(exactly = 0) { mockSkillRepository.remove(any()) }
    }

    companion object {
        fun createSkill(): Skill = Skill(
            name = "someName",
            level = 5,
            skillType = SkillType(
                description = "someDescription"
            )
        )
    }
}
