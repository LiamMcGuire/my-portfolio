package me.liammcguire.myportfolio.model

import java.time.Instant

data class Project(
    override val _id: String? = null,
    val title: String? = null,
    val description: String? = null,
    val shortDescription: String? = null,
    val repositoryLink: String? = null,
    val dateCompleted: Instant? = null,
    val latestProject: Boolean? = null
) : StorableModel, ValidatableModel
