package me.liammcguire.myportfolio.model

interface StorableModel {
    val _id: String?
}
