package me.liammcguire.myportfolio.model

data class Skill(
    override val _id: String? = null,
    val name: String? = null,
    val level: Int? = null,
    val skillType: SkillType? = null
) : StorableModel, ValidatableModel

data class SkillType(
    val description: String? = null
)
