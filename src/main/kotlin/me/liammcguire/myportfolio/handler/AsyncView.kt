package me.liammcguire.myportfolio.handler

import me.liammcguire.myportfolio.model.StorableModel
import me.liammcguire.myportfolio.repository.AsyncRepository
import me.liammcguire.myportfolio.utility.notFoundResponse
import me.liammcguire.myportfolio.utility.toOk
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

interface AsyncView<SM : StorableModel> {

    val repository: AsyncRepository<SM>

    fun view(request: ServerRequest): Mono<ServerResponse> = repository.fetchById(request.pathVariable("id"))
        .flatMap { model -> model.toOk() }
        .switchIfEmpty(notFoundResponse())
}
