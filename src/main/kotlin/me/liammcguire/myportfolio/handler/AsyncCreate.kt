package me.liammcguire.myportfolio.handler

import kotlin.reflect.KClass
import me.liammcguire.myportfolio.model.StorableModel
import me.liammcguire.myportfolio.model.ValidatableModel
import me.liammcguire.myportfolio.repository.AsyncRepository
import me.liammcguire.myportfolio.utility.toCreated
import me.liammcguire.myportfolio.utility.validate
import me.liammcguire.myportfolio.validation.Validator
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

interface AsyncCreate<SM : StorableModel, VM : ValidatableModel> {

    val repository: AsyncRepository<SM>
    val validator: Validator<VM>
    val validateType: KClass<VM>

    fun create(request: ServerRequest): Mono<ServerResponse> = request.bodyToMono(validateType.java)
        .validate(validator) { validatedModel ->
            repository.store(validatedModel.toStorableModel()).flatMap { storedModel ->
                storedModel.toCreated()
            }
        }

    fun VM.toStorableModel(): SM
}
