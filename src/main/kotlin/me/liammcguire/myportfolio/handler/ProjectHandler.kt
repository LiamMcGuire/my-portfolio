package me.liammcguire.myportfolio.handler

import kotlin.reflect.KClass
import me.liammcguire.myportfolio.model.Project
import me.liammcguire.myportfolio.repository.ProjectRepository
import me.liammcguire.myportfolio.utility.orDefault
import me.liammcguire.myportfolio.validation.ProjectValidator
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

class ProjectHandler(
    override val repository: ProjectRepository,
    override val validator: ProjectValidator
) : AsyncCreate<Project, Project>, AsyncUpdate<Project, Project>,
    AsyncList<Project>, AsyncView<Project>, AsyncDelete<Project> {

    override val validateType: KClass<Project> get() = Project::class

    override fun Project.toStorableModel(): Project = this

    override fun Project.mergeWith(validatedModel: Project): Project = this.copy(
        title = validatedModel.title orDefault title,
        description = validatedModel.description orDefault description,
        shortDescription = validatedModel.shortDescription orDefault shortDescription,
        repositoryLink = validatedModel.repositoryLink orDefault repositoryLink,
        dateCompleted = validatedModel.dateCompleted orDefault dateCompleted
    )
}

fun projectRoutes(projectHandler: ProjectHandler): RouterFunction<ServerResponse> = router {
    "/api/v1/project".nest {
        accept(MediaType.APPLICATION_JSON).nest {
            POST("", projectHandler::create)
            PATCH("/{id}/update", projectHandler::update)
        }
        GET("/list", projectHandler::list)
        GET("/{id}/view", projectHandler::view)
        DELETE("/{id}/delete", projectHandler::delete)
    }
}
