package me.liammcguire.myportfolio.handler

import kotlin.reflect.KClass
import me.liammcguire.myportfolio.model.StorableModel
import me.liammcguire.myportfolio.model.ValidatableModel
import me.liammcguire.myportfolio.repository.AsyncRepository
import me.liammcguire.myportfolio.utility.notFoundResponse
import me.liammcguire.myportfolio.utility.toOk
import me.liammcguire.myportfolio.utility.validate
import me.liammcguire.myportfolio.validation.Validator
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

interface AsyncUpdate<SM : StorableModel, VM : ValidatableModel> {

    val repository: AsyncRepository<SM>
    val validator: Validator<VM>
    val validateType: KClass<VM>

    fun update(request: ServerRequest): Mono<ServerResponse> = request.bodyToMono(validateType.java)
        .validate(validator, true) { validatedModel ->
            repository.fetchById(request.pathVariable("id")).flatMap { storedModel ->
                val updatedModel = storedModel.mergeWith(validatedModel)
                repository.store(updatedModel).flatMap { updatedModel.toOk() }
            }.switchIfEmpty(notFoundResponse())
        }

    fun SM.mergeWith(validatedModel: VM): SM
}
