package me.liammcguire.myportfolio.handler

import me.liammcguire.myportfolio.model.StorableModel
import me.liammcguire.myportfolio.repository.AsyncRepository
import me.liammcguire.myportfolio.utility.internalErrorResponse
import me.liammcguire.myportfolio.utility.notFoundResponse
import me.liammcguire.myportfolio.utility.okResponse
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

interface AsyncDelete<SM : StorableModel> {

    val repository: AsyncRepository<SM>

    fun delete(request: ServerRequest): Mono<ServerResponse> = repository.fetchById(request.pathVariable("id"))
        .flatMap { storedModel ->
            repository.remove(storedModel).flatMap { deleteResult ->
                okResponse().takeIf { deleteResult.deletedCount == 1L } ?: internalErrorResponse()
            }
        }
        .switchIfEmpty(notFoundResponse())
}
