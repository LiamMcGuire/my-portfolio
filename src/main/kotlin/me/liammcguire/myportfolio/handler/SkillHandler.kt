package me.liammcguire.myportfolio.handler

import kotlin.reflect.KClass
import me.liammcguire.myportfolio.model.Skill
import me.liammcguire.myportfolio.model.SkillType
import me.liammcguire.myportfolio.repository.SkillRepository
import me.liammcguire.myportfolio.utility.orDefault
import me.liammcguire.myportfolio.validation.SkillValidator
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

class SkillHandler(
    override val repository: SkillRepository,
    override val validator: SkillValidator
) : AsyncCreate<Skill, Skill>, AsyncUpdate<Skill, Skill>, AsyncList<Skill>,
    AsyncView<Skill>, AsyncDelete<Skill> {

    override val validateType: KClass<Skill> get() = Skill::class

    override fun Skill.toStorableModel(): Skill = this

    override fun Skill.mergeWith(validatedModel: Skill): Skill = this.copy(
        name = validatedModel.name orDefault name,
        level = validatedModel.level orDefault level,
        skillType = validatedModel.skillType?.let { partialSkillType ->
            SkillType(description = partialSkillType.description orDefault skillType?.description)
        } ?: skillType
    )
}

fun skillRoutes(skillHandler: SkillHandler): RouterFunction<ServerResponse> = router {
    "/api/v1/skill".nest {
        accept(MediaType.APPLICATION_JSON).nest {
            POST("", skillHandler::create)
            PATCH("/{id}/update", skillHandler::update)
        }
        GET("/list", skillHandler::list)
        GET("/{id}/view", skillHandler::view)
        DELETE("/{id}/delete", skillHandler::delete)
    }
}
