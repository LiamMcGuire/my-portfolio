package me.liammcguire.myportfolio.handler

import me.liammcguire.myportfolio.model.StorableModel
import me.liammcguire.myportfolio.repository.AsyncRepository
import me.liammcguire.myportfolio.utility.notFoundResponse
import me.liammcguire.myportfolio.utility.toOk
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

interface AsyncList<SM : StorableModel> {

    val repository: AsyncRepository<SM>

    fun list(request: ServerRequest): Mono<ServerResponse> = repository.list()
        .collectList().flatMap { models -> models.toOk().takeIf { models.isNotEmpty() } ?: notFoundResponse() }
}
