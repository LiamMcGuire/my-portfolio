package me.liammcguire.myportfolio.repository

import me.liammcguire.myportfolio.model.Skill
import org.springframework.data.mongodb.core.ReactiveMongoTemplate

class SkillRepository(template: ReactiveMongoTemplate) : AsyncRepository<Skill>(template, Skill::class)
