package me.liammcguire.myportfolio.repository

import me.liammcguire.myportfolio.model.Project
import org.springframework.data.mongodb.core.ReactiveMongoTemplate

class ProjectRepository(template: ReactiveMongoTemplate) : AsyncRepository<Project>(template, Project::class)
