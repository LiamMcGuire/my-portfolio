package me.liammcguire.myportfolio.repository

import com.mongodb.client.result.DeleteResult
import kotlin.reflect.KClass
import me.liammcguire.myportfolio.model.StorableModel
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

abstract class AsyncRepository<SM : StorableModel>(
    private val reactiveMongoTemplate: ReactiveMongoTemplate,
    private val type: KClass<SM>
) {

    fun store(model: SM): Mono<SM> = reactiveMongoTemplate.save(model)

    fun list(): Flux<SM> = reactiveMongoTemplate.findAll(type.java)

    fun fetchById(id: String): Mono<SM> = reactiveMongoTemplate.findById(id, type.java)

    fun remove(model: SM): Mono<DeleteResult> = reactiveMongoTemplate.remove(model)
}
