package me.liammcguire.myportfolio.utility

import me.liammcguire.myportfolio.model.StorableModel
import me.liammcguire.myportfolio.type.BasicFault
import me.liammcguire.myportfolio.type.ErrorResponse
import me.liammcguire.myportfolio.type.ValidationFault
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

fun okResponse(): Mono<ServerResponse> = ServerResponse.status(HttpStatus.OK).build()

fun notFoundResponse(): Mono<ServerResponse> = ServerResponse.status(HttpStatus.NOT_FOUND).build()

fun internalErrorResponse(): Mono<ServerResponse> = ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).build()

fun <SM : StorableModel> SM.toCreated(): Mono<ServerResponse> =
    ServerResponse.status(HttpStatus.CREATED)
        .contentType(MediaType.APPLICATION_JSON)
        .body(BodyInserters.fromValue(this))

fun <SM : StorableModel> SM.toOk(): Mono<ServerResponse> =
    ServerResponse.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .body(BodyInserters.fromValue(this))

fun <SM : StorableModel> List<SM>.toOk(): Mono<ServerResponse> =
    ServerResponse.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .body(BodyInserters.fromValue(this))

fun List<ValidationFault>.toBadRequest(): Mono<ServerResponse> =
    ServerResponse.status(HttpStatus.BAD_REQUEST)
        .contentType(MediaType.APPLICATION_JSON)
        .body(BodyInserters.fromValue(ErrorResponse(this)))

fun BasicFault.toBadRequest(): Mono<ServerResponse> =
    ServerResponse.status(HttpStatus.BAD_REQUEST)
        .contentType(MediaType.APPLICATION_JSON)
        .body(BodyInserters.fromValue(ErrorResponse(listOf(this))))
