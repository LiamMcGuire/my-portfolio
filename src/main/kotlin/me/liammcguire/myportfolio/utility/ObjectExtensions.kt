package me.liammcguire.myportfolio.utility

inline infix fun <reified A : Any> A?.orDefault(default: A?): A? = when (this is String) {
    true -> this.takeIf { !(it as String).isBlank() } as A? ?: default
    else -> this?.let { this } ?: default
}
