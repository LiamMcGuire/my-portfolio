package me.liammcguire.myportfolio.utility

import me.liammcguire.myportfolio.model.ValidatableModel
import me.liammcguire.myportfolio.type.BasicFault
import me.liammcguire.myportfolio.validation.Validator
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

fun <VM : ValidatableModel> Mono<VM>.validate(
    validator: Validator<VM>,
    excludeNullCheck: Boolean = false,
    onValid: (VM) -> Mono<ServerResponse>
): Mono<ServerResponse> = this.flatMap { model ->
    val validationResult = validator.validate(model, excludeNullCheck)
    validationResult.toBadRequest().takeIf { validationResult.isNotEmpty() } ?: onValid(model)
}.switchIfEmpty(BasicFault("The request did not have a JSON body.").toBadRequest())
