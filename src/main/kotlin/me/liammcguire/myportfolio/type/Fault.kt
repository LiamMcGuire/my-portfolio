package me.liammcguire.myportfolio.type

interface Fault {
    val cause: String
}

data class BasicFault(override val cause: String) : Fault

data class ValidationFault(val field: String, override val cause: String) : Fault
