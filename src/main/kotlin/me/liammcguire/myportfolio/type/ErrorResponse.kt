package me.liammcguire.myportfolio.type

import java.time.Instant

@Suppress("Unused")
data class ErrorResponse(val errors: List<Fault>) {
    val timestamp: Instant = Instant.now()
}
