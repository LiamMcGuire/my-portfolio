package me.liammcguire.myportfolio.validation

import me.liammcguire.myportfolio.model.ValidatableModel
import me.liammcguire.myportfolio.type.ValidationFault

abstract class Validator<VM : ValidatableModel> {

    fun validate(model: VM, excludeNullCheck: Boolean = false): List<ValidationFault> =
        validateFieldContents(model) + (validateNonNullFields(model).takeIf { !excludeNullCheck } ?: emptyList())

    protected abstract fun validateFieldContents(model: VM): List<ValidationFault>

    protected abstract fun validateNonNullFields(model: VM): List<ValidationFault>
}
