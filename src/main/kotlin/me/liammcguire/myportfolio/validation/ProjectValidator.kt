package me.liammcguire.myportfolio.validation

import java.time.Instant
import me.liammcguire.myportfolio.model.Project
import me.liammcguire.myportfolio.type.ValidationFault
import org.apache.commons.validator.routines.UrlValidator

class ProjectValidator : Validator<Project>() {

    override fun validateFieldContents(model: Project): List<ValidationFault> = listOfNotNull(
        ValidationFault("title", "The provided title exceeded the maximum length of 50.")
            .takeIf { model.title != null && model.title.length > 50 },
        ValidationFault("description", "The provided description exceeded the maximum length of 500.")
            .takeIf { model.description != null && model.description.length > 500 },
        ValidationFault("shortDescription", "The provided short description exceeded the maximum length of 200.")
            .takeIf { model.shortDescription != null && model.shortDescription.length > 200 },
        ValidationFault("repositoryLink", "The provided repository link was not valid.")
            .takeIf { model.repositoryLink != null && !UrlValidator().isValid(model.repositoryLink) },
        ValidationFault("dateCompleted", "The provided completion date must be in the past.")
            .takeIf { model.dateCompleted != null && model.dateCompleted.isAfter(Instant.now()) }
    )

    override fun validateNonNullFields(model: Project): List<ValidationFault> = listOfNotNull(
        ValidationFault("title", "A title was expected but was not provided.")
            .takeIf { model.title.isNullOrBlank() },
        ValidationFault("description", "A description was expected but was not provided.")
            .takeIf { model.description.isNullOrBlank() },
        ValidationFault("shortDescription", "A short description was expected but was not provided.")
            .takeIf { model.shortDescription.isNullOrBlank() },
        ValidationFault("repositoryLink", "A repository link was expected but was not provided.")
            .takeIf { model.repositoryLink.isNullOrBlank() }
    )
}
