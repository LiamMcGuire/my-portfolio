package me.liammcguire.myportfolio.validation

import me.liammcguire.myportfolio.model.Skill
import me.liammcguire.myportfolio.type.ValidationFault

class SkillValidator : Validator<Skill>() {

    override fun validateFieldContents(model: Skill): List<ValidationFault> = listOfNotNull(
        ValidationFault("name", "The provided name exceeded the maximum length of 50.")
            .takeIf { model.name != null && model.name.length > 50 },
        ValidationFault("level", "The provided level was not between 0 and 10.")
            .takeIf { model.level != null && model.level !in 1..10 },
        ValidationFault("skillType.description", "The provided description exceeded the maximum length of 50.")
            .takeIf { model.skillType?.description != null && model.skillType.description.length > 50 }
    )

    override fun validateNonNullFields(model: Skill): List<ValidationFault> = listOfNotNull(
        ValidationFault("name", "A name was expected but was not provided.")
            .takeIf { model.name.isNullOrBlank() },
        ValidationFault("level", "A level was expected but was not provided.")
            .takeIf { model.level == null },
        ValidationFault("skillType", "A skill type was expected but was not provided.")
            .takeIf { model.skillType == null },
        ValidationFault("skillType.description", "A description was expected but was not provided.")
            .takeIf { model.skillType != null && model.skillType.description.isNullOrBlank() }
    )
}
