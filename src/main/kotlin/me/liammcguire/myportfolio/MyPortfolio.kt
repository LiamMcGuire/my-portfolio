package me.liammcguire.myportfolio

import com.mongodb.ConnectionString
import me.liammcguire.myportfolio.handler.ProjectHandler
import me.liammcguire.myportfolio.handler.SkillHandler
import me.liammcguire.myportfolio.handler.projectRoutes
import me.liammcguire.myportfolio.handler.skillRoutes
import me.liammcguire.myportfolio.repository.ProjectRepository
import me.liammcguire.myportfolio.repository.SkillRepository
import me.liammcguire.myportfolio.validation.ProjectValidator
import me.liammcguire.myportfolio.validation.SkillValidator
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.support.beans
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.SimpleReactiveMongoDatabaseFactory
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService
import org.springframework.security.core.userdetails.ReactiveUserDetailsService
import org.springframework.security.core.userdetails.User
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.reactive.CorsWebFilter
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource

@SpringBootApplication
@EnableWebFluxSecurity
class Application

fun main(args: Array<String>) {
    runApplication<Application>(*args) {
        addInitializers(beans {
            bean<SecurityWebFilterChain> {
                ref<ServerHttpSecurity>()
                    .formLogin().disable()
                    .logout().disable()
                    .anonymous().disable()
                    .csrf().disable()
                    .httpBasic()
                    .and()
                    .authorizeExchange()
                    .pathMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                    .pathMatchers(HttpMethod.GET, "/**").permitAll()
                    .pathMatchers(HttpMethod.POST, "/**").authenticated()
                    .pathMatchers(HttpMethod.PATCH, "/**").authenticated()
                    .pathMatchers(HttpMethod.DELETE, "/**").authenticated()
                    .and()
                    .build()
            }

            bean<ReactiveUserDetailsService> {
                MapReactiveUserDetailsService(
                    User.withUsername("admin").password("{noop}admin").roles().build())
            }

            bean {
                CorsWebFilter(UrlBasedCorsConfigurationSource().apply {
                    registerCorsConfiguration("/**", CorsConfiguration().apply {
                        allowedOrigins = listOf("*")
                        allowedHeaders = listOf("Content-Type", "Authorization")
                        allowedMethods = listOf("OPTIONS", "GET", "POST", "PATCH", "DELETE")
                    })
                })
            }

            bean {
                ReactiveMongoTemplate(SimpleReactiveMongoDatabaseFactory(ConnectionString(
                    "mongodb://localhost:27017/my-portfolio"
                )))
            }

            bean { ProjectRepository(ref()) }
            bean { SkillRepository(ref()) }

            bean { ProjectValidator() }
            bean { SkillValidator() }

            bean { ProjectHandler(ref(), ref()) }
            bean { SkillHandler(ref(), ref()) }

            bean { projectRoutes(ref()) }
            bean { skillRoutes(ref()) }
        })
    }
}
